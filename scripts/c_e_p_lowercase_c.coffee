# Description:
#   cEP is always written with a lower case c.
#
# Author:
#   Vamshi Krishna(@Vamshi99)

module.exports = (robot) ->

  robot.hear /(^|[^\w])CEP($|[^\w])/, (msg) ->

    emoji = ":triumph: :angry: :expressionless: :open_mouth: :confounded: :anguished: :no_mouth: :disappointed_relieved:".split(" ")
    rand_emoji = emoji[(Math.random() * emoji.length) | 0]

    msg.send "cEP is always written with a lower case c. #{rand_emoji}"
